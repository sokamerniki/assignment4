# Assignment 3
Group Name: Sokamerniki

## Members

- Gabdylgaziz Zhagypar
- Sabir Glazhdin
- Yerasyl Rymkul
- Nurdaulet Toleugaliev
- Yerkhan Sarshaev


## Installation and Execute

Use the Docker to launch the project.

```bash
docker build -t front ./fruits-kz
docker build -t backend ./fruits-kz-back
docker build -t etl ./etl-job
docker compose up
```

## To check logs

```bash
docker ps #to check running docker containers
docker logs <id-of-container>
```


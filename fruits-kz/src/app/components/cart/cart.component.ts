import {Component, OnInit} from '@angular/core';
import { BackendService } from 'src/app/backend.service';
import { Product, products, selected_fruits} from "../../models/products";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
    products : Product[] = selected_fruits;
    fruits: Product[] = [];

    constructor(private backendService: BackendService) {

    }

    ngOnInit() {
    }

    getTotalPrice(): number {
        let price = 0;
        this.products.forEach(
            (product) => {
                if(product.count) {
                    price += product.count * product.price;
                }
            }
        )
        return price
    }

    getDescription(): string {
        let check = "";
        this.products.forEach(
            (fruit) => {
                check += fruit.name + " " + fruit.count + "\n"
            }
        )
        return check
    }

    showAlert(): void {
        let total_price = this.getTotalPrice()
        let message = "Вы сделали заказ на сумму " + total_price + "₸"
        this.backendService.postOrder(total_price, this.getDescription())
        alert(message)
    }

}

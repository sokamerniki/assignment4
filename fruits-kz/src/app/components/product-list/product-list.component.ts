import { Component, OnInit } from '@angular/core';
import { BackendService } from 'src/app/backend.service';
import { Product, products, selected_fruits } from 'src/app/models/products';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  selected_products: Product[] = selected_fruits;
  filtered_prod = [...products];
  fruits: Product[];

  constructor(private backendService: BackendService) {
    this.fruits = [];
  }

  // будет категория selectedCategory : string = "None"


  ngOnInit() {
    this.addFruits()
  }

  addFruits() {
    this.backendService.getFruits().subscribe(
      (fruits) => {
        this.fruits = fruits
      },

      (error) => {
        console.error(error)
      }
    )
  }


  addProduct(product: Product) {
      if (!product.count) {
          product.count = 1;
          this.selected_products.push(product);
          return
      }
      product.count += 1;
  }

  decreaseProduct(product: Product) {
    this.selected_products = this.selected_products.filter (
      (value) => {
          return value.count != undefined;
      }
    )
      if(!product.count) {
          return;
      }
      product.count -= 1;
  }
  getProductDescribe(product: Product){
    return `${product.price} ₸/кг`;;
  }
}

